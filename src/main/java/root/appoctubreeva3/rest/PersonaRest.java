/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.appoctubreeva3.rest;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.appoctubreeva3.dao.PersonasJpaController;
import root.appoctubreeva3.dao.exceptions.NonexistentEntityException;
import root.appoctubreeva3.entity.Personas;

/**
 *
 * @author ccruces
 */
@Path("persona")
public class PersonaRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listaPersonas() {

        PersonasJpaController dao = new PersonasJpaController();

        List<Personas> lista = dao.findPersonasEntities();
        return Response.ok(200).entity(lista).build();

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Personas persona) {

        PersonasJpaController dao = new PersonasJpaController();

        try {
            dao.create(persona);
        } catch (Exception ex) {
            Logger.getLogger(PersonaRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(persona).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(Personas persona) {
        PersonasJpaController dao = new PersonasJpaController();

        try {
            dao.edit(persona);
        } catch (Exception ex) {
            Logger.getLogger(PersonaRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(persona).build();
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{rut}")
    public Response eliminar(@PathParam("rut") String rut) {

        try {
            PersonasJpaController dao = new PersonasJpaController();

            dao.destroy(rut);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(PersonaRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok("persona eliminada").build();
    }

}
