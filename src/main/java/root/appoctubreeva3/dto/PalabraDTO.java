/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.appoctubreeva3.dto;

/**
 *
 * @author ccruces
 */
public class PalabraDTO {
    
  private String palabra;
  private String significado;


 // Getter Methods 

  public String getPalabra() {
    return palabra;
  }

  public String getSignificado() {
    return significado;
  }

 // Setter Methods 

  public void setPalabra( String palabra ) {
    this.palabra = palabra;
  }

  public void setSignificado( String significado ) {
    this.significado = significado;
  }
}